import colorgram
import turtle as turtle_module
from turtle import Turtle, Screen
import random

# colors_in_image = []
#
#
# def extract_colors_from_img():
#     global colors_in_image
#     colors = colorgram.extract("image.jpg", 30)
#
#     for rgb_color in colors:
#         colors_in_image.append((rgb_color.rgb.r,
#                                 rgb_color.rgb.g,
#                                 rgb_color.rgb.b))
#
#     print(colors_in_image)
#
#
# extract_colors_from_img()

colors = [(250, 251, 248), (199, 169, 94), (227, 239, 232), (128, 179, 191), (163, 57, 78), (234, 221, 120),
          (49, 113, 167), (242, 218, 224), (105, 87, 83), (240, 245, 250), (142, 187, 119), (216, 151, 171),
          (67, 125, 76), (94, 124, 181), (84, 165, 94), (192, 69, 90), (162, 34, 50), (144, 119, 114), (222, 172, 182),
          (208, 116, 44), (160, 203, 212), (174, 205, 176), (75, 60, 56), (175, 190, 214), (67, 56, 52),
          (216, 180, 178), (82, 141, 168)]

paint_brush = Turtle()
paint_brush.shape("circle")
paint_brush.speed("fastest")
turtle_module.colormode(255)
paint_brush.hideturtle()
paint_brush.penup()
paint_brush.setheading(225)
paint_brush.forward(300)
paint_brush.setheading(0)
paint_brush.pendown()


def create_walk():
    global paint_brush

    paint_brush.dot(20, random.choice(colors))

    for _ in range(5):
        paint_canvas()

        paint_brush.setheading(90)
        paint_brush.up()
        paint_brush.forward(50)
        paint_brush.left(90)

        paint_canvas()

        paint_brush.setheading(90)
        paint_brush.up()
        paint_brush.forward(50)
        paint_brush.right(90)


def paint_canvas():
    paint_brush.up()
    for _ in range(10):
        paint_brush.dot(20, random.choice(colors))
        paint_brush.forward(50)
        paint_brush.dot(20, random.choice(colors))


create_walk()

my_screen = Screen()
my_screen.exitonclick()
